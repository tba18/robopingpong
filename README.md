Created by: Bo Anderson

To run the main code base, due to requirements and dependencies, move the 'Manipulator-Mujoco" folder out of 'RoboPingPong' and onto your Desktop. Then, go into Manipulator-Mujoco/demo and run the command 'mjpython ur5eRL.py' for the project's RL training model.

This repository is adapted from the Manipulator Mujoco project created by Ian Chuang. All credit for the basic environment setup and Mujoco simulation goes to the repository found here: - [ian-chuang/Manipulator-Mujoco](https://github.com/ian-chuang/Manipulator-Mujoco.git)
