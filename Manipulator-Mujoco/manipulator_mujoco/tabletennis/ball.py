from dm_control import mjcf
from manipulator_mujoco.utils.transform_utils import (
    mat2quat
)
import numpy as np

class Ball():
    def __init__(self, xml_path, body_names = None, name: str = None):
        self._mjcf_root = mjcf.from_path(xml_path)
        if name:
            self._mjcf_root.model = name

            # Find MJCF elements that will be exposed as attributes.
        if body_names is None:
            self._body = self.mjcf_model.find_all('body')
        else:
            self._body = [self._mjcf_root.find('body', name) for name in body_names]

    @property
    def body(self):
        """List of body elements belonging to the ball."""
        return self._body

    @property
    def mjcf_model(self):
        """Returns the `mjcf.RootElement` object corresponding to this table."""
        return self._mjcf_root
    
    # def attach_tool(self, child, pos: list = [0, 0, 0], quat: list = [1, 0, 0, 0]) -> mjcf.Element:
    #     frame = self._attachment_site.attach(child)
    #     frame.pos = pos
    #     frame.quat = quat
    #     return frame