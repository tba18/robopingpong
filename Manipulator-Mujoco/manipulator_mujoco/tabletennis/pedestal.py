from dm_control import mjcf
from manipulator_mujoco.utils.transform_utils import (
    mat2quat
)
import numpy as np

class Pedestal():
    def __init__(self, xml_path, name: str = None):
        self._mjcf_root = mjcf.from_path(xml_path)
        if name:
            self._mjcf_root.model = name

    @property
    def mjcf_model(self):
        """Returns the `mjcf.RootElement` object corresponding to this table."""
        return self._mjcf_root
    
    # def attach_tool(self, child, pos: list = [0, 0, 0], quat: list = [1, 0, 0, 0]) -> mjcf.Element:
    #     frame = self._attachment_site.attach(child)
    #     frame.pos = pos
    #     frame.quat = quat
    #     return frame