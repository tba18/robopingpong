import time
import os
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
from dm_control import mjcf
import mujoco.viewer
import gymnasium as gym
from gymnasium import spaces
from manipulator_mujoco.arenas import StandardArena
from manipulator_mujoco.robots import Arm
from manipulator_mujoco.tabletennis import Table
from manipulator_mujoco.tabletennis import Pedestal
from manipulator_mujoco.tabletennis import Ball
from manipulator_mujoco.mocaps import Target
from manipulator_mujoco.controllers import OperationalSpaceController
import gymnasium
import manipulator_mujoco
import seaborn as sns
from matplotlib.patches import Patch
import random
from tqdm import tqdm

#####! ORIGINAL ADAPTATION OF BASIC MUJOCO SIMULATION
# Create the environment with rendering in human mode
# env = gymnasium.make('manipulator_mujoco/UR5eEnv-v0') #, render_mode='human'
# #####~
# done = False
# observation, info = env.reset()

# action = env.action_space.sample()
# observation, reward, terminated, truncated, info = env.step(action)

#####!
# Reset the environment with a specific seed for reproducibility
#observation, info = env.reset(seed=42)

# Run simulation for a fixed number of steps
# for _ in range(1000):
# i = 0
# while i < 10:

#     # Choose a random action from the available action space
#     action = env.action_space.sample()

#     # Take a step in the environment using the chosen action
#     observation, reward, terminated, truncated, info = env.step(action)

#     # Check if the episode is over (terminated) or max steps reached (truncated)
#     if terminated or truncated:
#         # If the episode ends or is truncated, reset the environment
#         observation, info = env.reset()
#         i += 1

# # Close the environment when the simulation is done
# env.close()
#####~

import tensorflow as tf
from tensorflow.keras import layers, models, optimizers
from collections import deque

# Define the DQN model
def build_dqn(input_shape,):
    model = models.Sequential([
        layers.Input(shape=input_shape),
        layers.Dense(128, activation='relu'),
        layers.Dense(64, activation='relu'),
        layers.Dense(3, activation='linear')  # Output layer with linear activation for Q-values
    ])
    model.compile(optimizer=optimizers.Adam(learning_rate=0.001), loss='mse')  # Mean Squared Error loss for Q-learning
    return model

# Environment parameters
input_shape = (3,)  # Assuming your state representation is a 3D position

# Build the DQN model
dqn_model = build_dqn(input_shape,)
dqn_target = build_dqn(input_shape,)
dqn_target.set_weights(dqn_model.get_weights())

replay_buffer = deque(maxlen=10000)
# Other training parameters
discount_factor = 0.99  # Discount factor for future rewards
epsilon = 1.0
optimizer = optimizers.Adam(learning_rate=0.001)  # Adjust learning rate as needed

# Training loop
num_episodes = 10000
rwrds = []
avg_rwrds = []
episodes = []
losses = []
ep10 = []
q_preds_x = []
q_preds_y = []
q_preds_z = []

env = gymnasium.make('manipulator_mujoco/UR5eEnv-v0') #, render_mode='human'
    #####~
done = False
observation, info = env.reset()
target_pos = observation[:3]
init_pos = np.array(info[0])

for episode in range(num_episodes + 1):
    # Run an episode and collect experiences
    # Your RL code here
    done = False

    while not done:

        action = target_pos
        #action = env.action_space.sample()
        next_obs, reward, terminated, truncated, info = env.step(action)

        # Update DQN with Q-learning
        # Your Q-learning update code here
            # Update DQN with Q-learning
        batch_size = 1  # Adjust based on your requirements
        if terminated or truncated:
            # Sample a batch of experiences from the replay buffer
            replay_buffer.append((init_pos, target_pos, reward))
            if episode % 10 == 0 and len(replay_buffer) >= batch_size:
                batch = random.sample(replay_buffer, batch_size)

            # Extract components from the batch
                init_poxs, target_poxs, rewards = zip(*batch)

            # Convert to NumPy arrays
                init_poxs = np.array(init_poxs)
                target_poxs = np.array(target_poxs)
                rewards = np.array(rewards)

            # Calculate target Q-values (target Q-network used for stability)
                target_q_values = dqn_target.predict(init_poxs)
                target_q_values = rewards + discount_factor * np.max(target_q_values, axis=1)

            # Update the main DQN network using the Q-learning loss
                with tf.GradientTape() as tape:
                    q_values = dqn_model(init_poxs)
                    selected_q_values = tf.reduce_sum(q_values * target_poxs, axis=1)
                    loss = tf.reduce_mean(tf.square(target_q_values - selected_q_values))

                q_values_predicted = dqn_model.predict(init_poxs)
                #print(q_values_predicted)

                gradients = tape.gradient(loss, dqn_model.trainable_variables)
                optimizer.apply_gradients(zip(gradients, dqn_model.trainable_variables))
                losses = np.append(losses, loss)
                q_preds_x = np.append(q_preds_x, q_values_predicted[0][0])
                q_preds_y = np.append(q_preds_y, q_values_predicted[0][1])
                q_preds_z = np.append(q_preds_z, q_values_predicted[0][2])
                ep10 = np.append(ep10, episode)
            #print(f"Episode: {episode}, Total Reward: {reward}")
            rwrds = np.append(rwrds, reward)
            add = 0
            for r in rwrds:
                add += r
            avg_rwrds = np.append(avg_rwrds, add / len(rwrds))
            episodes = np.append(episodes, episode)
            observation, info = env.reset()
            target_pos = observation[:3]
            init_pos = np.array(info[0])
            done = True


# # Plot the learning progress

plt.plot(episodes, avg_rwrds)
plt.xlabel('Episode')
plt.ylabel('Average Reward')
plt.title('Average Rewards Learning Progress')

home_dir = os.path.expanduser("~")

# Specify the path to the Desktop
desktop_path = os.path.join(home_dir, "Desktop")

# Specify the full path to the image file on the Desktop
image_file_path1 = os.path.join(desktop_path, "learning_progress.png")
plt.savefig(image_file_path1)
plt.close()

plt.plot(ep10, losses)
plt.xlabel('Episode')
plt.ylabel('Losses')
plt.title('Training Loss Over 10000 Episodes')

image_file_path2 = os.path.join(desktop_path, "training_loss.png")
plt.savefig(image_file_path2)
plt.close()

plt.plot(ep10, q_preds_x, label='X Coordinate')
plt.plot(ep10, q_preds_y, label='Y Coordinate')
plt.plot(ep10, q_preds_z, label='Z Coordinate')
plt.xlabel('Episode')
plt.ylabel('Coordinate Value')
plt.title('Target Values Predicted Over 10000 Episodes')
plt.legend()

image_file_path3 = os.path.join(desktop_path, "q_predicts.png")
plt.savefig(image_file_path3)

# Close the plot to prevent display in non-interactive environments
plt.close()